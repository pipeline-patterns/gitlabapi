"""
We store all API Calls (not executed from cURL) from this library.
"""
import os
import requests
import sys

GITLAB_ORIGIN = 'https://gitlab.com'
GITLAB_API_VERSION = 'v4'


class GitLabAPIException(BaseException):
    """Raised when http exceptions occur"""
    pass


class GitLab(object):

    def __init__(self, origin=None, api_version=None, **kwargs):
        if origin is None:
            origin = GITLAB_ORIGIN
        self.origin = origin

        if api_version is None:
            api_version = GITLAB_API_VERSION
        self.api_version = api_version

        self.trigger_token = kwargs.pop('trigger_token', None)
        self.private_token = kwargs.pop('private_token', None)
        self.job_token = kwargs.pop('job_token', None)

    def create_issue(self, project_id, title, description):
        path = f'projects/{project_id}/issues'
        data = {
            'title': title,
            'description': description,
        }
        return self.__call_gitlab(path, http_method='POST', data=data)

    def list_project_branches(self, project_id):
        """
        https://docs.gitlab.com/ce/api/branches.html
        :param project_id:
        :return:
        """
        path = f'projects/{project_id}/repository/branches'
        return self.__call_gitlab(path, self.private_token, private_token=True)

    def get_project_branch(self, project_id, branch_name):
        """
        Used for testing only at the moment.
        :param project_id:
        :param branch_name:
        :return:
        """
        path = f'projects/{project_id}/repository/branches/{branch_name}'
        return self.__call_gitlab(path, self.private_token, private_token=True)

    def create_branch(self, project_id, target_branch_name, source_branch_name=None):
        """
        :param project_id:
        :param branch_name:
        :param source_branch_name:
        :return:
        """
        if source_branch_name is None:
            source_branch_name = 'master'
        path = f'projects/{project_id}/repository/branches?branch={target_branch_name}&ref={source_branch_name}'
        method = 'POST'
        return self.__call_gitlab(path, self.private_token, http_method=method, private_token=True)

    def list_project_merge_requests(self, project_id, private_token=False, **kwargs):
        """
        Job Token or Private Token are both acceptable
        :param project_id:
        :param kwargs:
        :return:
        """
        search_attributes = ''
        if kwargs:
            search_attributes = '?' + '&'.join(['{}={}'.format(k, v) for k, v in kwargs.items()])
        path = f'projects/{project_id}/merge_requests' + search_attributes
        token = self.private_token if private_token else self.job_token
        return self.__call_gitlab(path, token, private_token=private_token)


    def create_merge_request(self, project_id, source_branch_name, target_branch_name, title):
        path = f'projects/{project_id}/merge_requests'
        method = 'POST'
        data = {
            'source_branch': source_branch_name,
            'target_branch': target_branch_name,
            'title': title,
        }
        return self.__call_gitlab(path, self.private_token, data=data, http_method=method, private_token=True)

    def create_merge_request_note(self, project_id, merge_request_iid, body):
        path = f'projects/{project_id}/merge_requests/{merge_request_iid}/notes'
        method = 'POST'
        data = {
            'body': body,
        }
        return self.__call_gitlab(path, self.private_token, data=data, http_method=method, private_token=True)

    def list_pipeline_jobs(self, ci_job_token, project_id, pipeline_id, *scopes):
        path = f'projects/{project_id}/pipelines/{pipeline_id}/jobs'
        if scopes:
            path += '?' + '&'.join([f'scope[]={x}' for x in scopes])
        data = self.__call_gitlab(path, ci_job_token)
        return data

    def create_tag(self, project_id, project_ref, tag_name, message=None, release_notes=None):
        """
        appears to require private token.
        :param project_id:
        :param project_ref:
        :param tag_name:
        :param message:
        :param release_notes:
        :return:
        """
        path = f'projects/{project_id}/repository/tags'

        payload = {
            'tag_name': tag_name,
            'ref': project_ref,
        }
        if message:
            payload['message'] = message
        if release_notes:
            payload['release_description'] = release_notes

        response = self.__call_gitlab(path, self.private_token, http_method='post', data=payload, private_token=True)
        print(response)

    def play_job(self, project_id, job_id):
        """
        For some reason this only accepts private tokens.
        """
        path = f"projects/{project_id}/jobs/{job_id}/play"
        data = self.__call_gitlab(path, self.private_token, http_method='post', private_token=True,)
        return data

    def download_artifact(self, project_id, job_id, artifact_name, artifact_path=None):
        if artifact_path:
            path = f'{project_id}/jobs/{job_id}/artifacts/{artifact_path.strip("/")}/{artifact_name.strip("/")}'
        else:
            path = f'{project_id}/jobs/{job_id}/artifacts/{artifact_name.strip("/")}'
        response = self.__call_gitlab(path, self.private_token, private_token=True, raw_response=True)
        with open(artifact_name, 'wb') as f:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

    def get_merge_request(self, project_id, merge_request_iid):
        path = f'projects/{project_id}/merge_requests/{merge_request_iid}'

        response = self.__call_gitlab(path, self.job_token)
        return response

    def __call_gitlab(self, path, token, default_headers=None, data=None, http_method=None, private_token=False,
                      api_version=None, raw_response=False):
        """
        All API Calls to GitLab flow through here.
        :param path: The URL without hostname nor leading/trailing forward-slashes.
        :param token: The token
        :param default_headers:
        :param data:
        :param http_method:
        :param private_token:
        :param api_version:
        :param raw_response:
        :return:
        """
        origin = self.origin
        if api_version is None:
            api_version = self.api_version
        url = f'{origin.strip("/")}/api/{api_version.strip("/")}/{path.strip("/")}'

        if http_method is None:
            if token is None:
                http_method = 'post'
            else:
                http_method = 'get'

        http_method = http_method[:].lower()

        if default_headers is None:
            headers = {}
        else:
            headers = default_headers.copy()

        if private_token:
            headers.update({
                'PRIVATE-TOKEN': token,
            })
        else:
            headers.update({
                'JOB-TOKEN': token,
            })

        print('******URL******')
        print(url)
        print('----HEADERS----')
        print(headers)
        print('-----DATA------')
        print(data)
        response = getattr(requests, http_method)(url, headers=headers, data=data)
        print(response.status_code)
        print(response.content)


        def get_error_message(x):
            return x.json().get('message', response.content)

        # https://docs.gitlab.com/ce/user/project/new_ci_build_permissions_model.html#prerequisites-to-use-the-new-permissions-model
        if response.status_code in [500]:
            # Check if you have another web proxy sitting in front of NGINX
            # (HAProxy, Apache, etc.). It might be a good idea to let GitLab
            # use the internal NGINX web server and not disable it completely.
            # See this comment for an example.
            raise GitLabAPIException('Server is misconfigured: %d' % response.status_code)
        elif response.status_code in [404]:
            # There's a good chance that the user is not logged in.
            raise GitLabAPIException('The page does not exist or you are using '
                                     'any authentication credentials: %d' % response.status_code)
        elif response.status_code in [403, 401]:
            # Make sure you are a member of the group or project you're trying
            # to have access to. As an Administrator, you can verify that by
            # impersonating the user and retry the failing job in order to
            # verify that everything is correct.
            if private_token or token is None:
                raise GitLabAPIException(
                    'HTTP %d: %s' % (response.status_code, get_error_message(response))
                )
            elif 'CI_PRIVATE_TOKEN' in os.environ:
                if 'JOB-TOKEN' in headers:
                    headers.pop('JOB-TOKEN', None)
                    headers = {
                        'PRIVATE-TOKEN': os.environ['CI_PRIVATE_TOKEN'],
                    }
                print('JOB-TOKEN usage failed, attempting to use PRIVATE-TOKEN')
                response = getattr(requests, http_method)(url, headers=headers, data=data)
                if response.status_code in [200, 201]:
                    return response if raw_response else response.json()
                raise GitLabAPIException(get_error_message(response))
            else:
                raise GitLabAPIException(
                    '%s: Try setting a Personal Access Token as CI_PRIVATE_TOKEN Secret '
                    'Variable as a fix to this issue.' % get_error_message(response)
                )
        elif response.status_code in [200, 201]:
            return response if raw_response else response.json()
        else:
            print(get_error_message(response))
            raise GitLabAPIException('Unsure how to interpret this response code: %d' % response.status_code)



